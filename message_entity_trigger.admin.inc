<?php
/**
 * @file
 * Administration forms for the Message Entity Trigger module.
 */

/**
 * Form constructor for the message_entity_trigger_settings_form.
 *
 * @see message_entity_trigger_settings_form_validate()
 * @see message_entity_trigger_settings_form_submit()
 * @ingroup forms
 */
function message_entity_trigger_settings_form($form, &$form_state) {
  $form = array();

  return $form;
}

/**
 * Validation handler for message_entity_trigger_settings_form form.
 *
 * @see message_entity_trigger_settings_form()
 */
function message_entity_trigger_settings_form_validate($form, &$form_state) {
}


/**
 * Submit handler for message_entity_trigger_settings_form form.
 *
 * @see message_entity_trigger_settings_form()
 */
function message_entity_trigger_settings_form_submit($form, &$form_state) {
}

/**
 * Form constructor for the message_entity_trigger add/edit form.
 *
 * @see message_entity_trigger_form_validate()
 * @see message_entity_trigger_form_submit()
 * @see message_entity_trigger_form_delete_submit()
 * @ingroup forms
 */
function message_entity_trigger_form($form, &$form_state, $entity) {
  $form = array();
  $id = uniqid('message_entity_trigger_');
  $entity_info = entity_get_info();
  $message_types = message_type_load();
  $entity_type = isset($form_state['values']['entity_type']) ? $form_state['values']['entity_type'] : (isset($entity->entity_type) ? $entity->entity_type : key($entity_info));
  $bundle = isset($form_state['values']['bundle']) ? $form_state['values']['bundle'] : (isset($entity->bundle) ? $entity->bundle : key($entity_info[$entity_type]['bundles']));

  if (!$message_types) {
    drupal_set_message(t('Please first create a !link.', array('!link' => l('message type', 'admin/structure/messages'))), 'warning');
    return $form;
  }

  // Set the form id attribute for ajax refresh.
  $form['#id'] = $id;

  // Add form elements.
  $form['entity_type'] = array(
    '#type' => 'select',
    '#title' => t('Entity Type'),
    '#default_value' => $entity_type,
    '#options' => drupal_map_assoc(array_keys($entity_info)),
    '#ajax' => array(
      'callback' => 'message_entity_trigger_form_ajax',
      'wrapper' => $id,
      'method' => 'replace',
      'effect' => 'fade',
    ),
  );
  $form['bundle'] = array(
    '#type' => 'select',
    '#title' => t('Bundle'),
    '#default_value' => $bundle,
    '#options' => drupal_map_assoc(array_keys($entity_info[$entity_type]['bundles'])),
  );
  $form['hook'] = array(
    '#type' => 'select',
    '#title' => t('Hook'),
    '#default_value' => isset($entity->hook) ? $entity->hook : '',
    '#options' => drupal_map_assoc(array(
      'insert',
      'presave',
      'update',
      'delete',
    )),
  );
  $form['message_type'] = array(
    '#type' => 'select',
    '#title' => t('Message Type'),
    '#default_value' => isset($entity->message_type) ? $entity->message_type : '',
    '#options' => drupal_map_assoc(array_keys($message_types)),
  );
  $form['actions'] = array(
    '#type' => 'actions',
    'submit' => array(
      '#type' => 'submit',
      '#value' => t('Save Trigger'),
    ),
  );

  return $form;
}

/**
 * Validation handler for message_entity_trigger_add_form form.
 */
function message_entity_trigger_form_ajax(&$form, &$form_state) {
  return $form;
}

/**
 * Submit handler for message_entity_trigger_add_form form.
 */
function message_entity_trigger_form_submit(&$form, &$form_state) {
  $entity = $form_state['message_entity_trigger'];

  $entity->entity_type = $form_state['values']['entity_type'];
  $entity->bundle = $form_state['values']['bundle'];
  $entity->hook = $form_state['values']['hook'];
  $entity->message_type = $form_state['values']['message_type'];

  entity_save('message_entity_trigger', $entity);

  $form_state['redirect'] = 'admin/config/system/message_entity_triggers';
}
