<?php
/**
 * @file
 * Install, update and uninstall functions for the message_entity_trigger
 * module.
 */

/**
 * Implements hook_schema().
 */
function message_entity_trigger_schema() {
  $schema = array();

  $schema['message_entity_trigger'] = array(
    'description' => 'The base table for the message entity trigger.',
    'fields' => array(
      'id' => array(
        'description' => 'Primary key of the message entity trigger.',
        'type' => 'serial',
        'unsigned' => TRUE,
        'not null' => TRUE,
      ),
      'entity_type' => array(
        'description' => 'The entity type.',
        'type' => 'varchar',
        'length' => 32,
        'not null' => TRUE,
        'default' => '',
      ),
      'bundle' => array(
        'description' => 'The entity bundle.',
        'type' => 'varchar',
        'length' => 32,
        'not null' => TRUE,
        'default' => '',
      ),
      'hook' => array(
        'description' => 'The hook that triggers the message.',
        'type' => 'varchar',
        'length' => 255,
        'not null' => TRUE,
        'default' => '',
      ),
      'message_type' => array(
        'description' => 'The unified identifier for a message type.',
        'type' => 'varchar',
        'length' => 255,
        'not null' => TRUE,
        'default' => '',
      ),
      'name' => array(
        'description' => 'The machine name for EntityAPIControllerExportable.',
        'type' => 'varchar',
        'length' => 255,
        'not null' => TRUE,
        'default' => '',
      ),
      'module' => array(
        'description' => 'The name of the providing module if the entity has been defined in code.',
        'type' => 'varchar',
        'length' => 255,
        'not null' => FALSE,
      ),
      'status' => array(
        'type' => 'int',
        'not null' => TRUE,
        'default' => 0x01,
        'size' => 'tiny',
        'description' => 'The exportable status of the entity.',
      ),
    ),
    'primary key' => array('id'),
  );

  return $schema;
}
